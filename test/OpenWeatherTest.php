<?php
declare(strict_types=1);

namespace RouteeOwpApi\Tests;

use RouteeBaseApi\Exception\MissingRequiredFieldException;
use RouteeOwpApi\Entity\CloudsEntity;
use RouteeOwpApi\Entity\CoordEntity;
use RouteeOwpApi\Entity\CurrentWeatherEntity;
use RouteeOwpApi\Entity\MainEntity;
use RouteeOwpApi\Entity\SysEntity;
use RouteeOwpApi\Entity\WeatherEntity;
use RouteeOwpApi\Entity\WindEntity;
use RouteeOwpApi\Helpers\EnvHelper;
use RouteeOwpApi\Request\CurrentWeatherRequest;

class OpenWeatherTest extends BaseRequestTest
{
    /**
     * @var CurrentWeatherRequest|null
     */
    protected $request = null;
    protected function setUp():void
    {
        parent::setUp();
        $this->request = new CurrentWeatherRequest();
    }

    public function testAppIdIsMissing()
    {
        $this->expectException(MissingRequiredFieldException::class);
        $response = $this->request->getByCityName("Thessaloniki");
    }

    public function testAppIdFromParams()
    {
        EnvHelper::setOpenWeatherApiKey(null);
        $this->request->setAppId("debbb6f06584c4422edc5a0dfe1547f5");
        $response = $this->request->getByCityName("Thessaloniki");
        $this->assertTrue($response->isSuccess());
    }

    public function testAppIdFromEnv()
    {
        EnvHelper::setOpenWeatherApiKey("debbb6f06584c4422edc5a0dfe1547f5");
        $response = $this->request->getByCityName("Thessaloniki");
        $this->assertTrue($response->isSuccess());
    }


    public function testGetByCityName()
    {
        $response = $this->request->getByCityName("Thessaloniki");
        $city = $response->getDataFromJson()['name'];
        $this->assertEquals("Thessaloniki", $city);
    }
    public function testGetByCityId()
    {
        $response = $this->request->getByCityId(734077);
        $city = $response->getDataFromJson()['name'];
        $this->assertEquals("Thessaloniki", $city);
    }
    public function testGetByCityCoord()
    {
        $response = $this->request->getByCoord(40.6403, 22.9439);
        $city = $response->getDataFromJson()['name'];
        $this->assertEquals("Thessaloniki", $city);
    }
    public function testGetByZipCode()
    {
        $response = $this->request->getByZipCode("94040,us");
        $city = $response->getDataFromJson()['name'];
        $this->assertEquals("Mountain View", $city);
    }
    public function testCityNotFound()
    {
        $response = $this->request->getByZipCode("57019,gr");
        $this->assertFalse($response->isSuccess());
        $this->assertEquals($response->getStatusCode(), 404);
    }

    public function testEntity()
    {
        $response = $this->request->getByCityName("Thessaloniki");
        /**
         * @var CurrentWeatherEntity $entity
         */
        $entity = $response->toEntity();
        $this->assertInstanceOf(CoordEntity::class, $entity->coord);
        $this->assertInstanceOf(WeatherEntity::class, $entity->weather);
        $this->assertInstanceOf(MainEntity::class, $entity->main);
        $this->assertInstanceOf(WindEntity::class, $entity->wind);
        $this->assertInstanceOf(CloudsEntity::class, $entity->clouds);
        $this->assertInstanceOf(SysEntity::class, $entity->sys);
        $this->assertEquals("Thessaloniki", $entity->name);
    }
}