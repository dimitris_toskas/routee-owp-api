<?php
declare(strict_types=1);

namespace RouteeOwpApi\Entity;

class MainEntity extends BaseEntity
{
    public $temp;
    public $feels_like;
    public $temp_min;
    public $temp_max;
    public $pressure;
    public $humidity;
}