<?php
declare(strict_types=1);

namespace RouteeOwpApi\Entity;

class WeatherEntity extends BaseEntity
{
    public $id;
    public $main;
    public $description;
    public $icon;
}