<?php
declare(strict_types=1);

namespace RouteeOwpApi\Entity;

class SysEntity extends BaseEntity
{
    public $type;
    public $id;
    public $country;
    public $sunrise;
    public $sunset;
}