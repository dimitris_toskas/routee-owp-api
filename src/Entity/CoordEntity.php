<?php
declare(strict_types=1);

namespace RouteeOwpApi\Entity;

class CoordEntity extends BaseEntity
{
    public $lat;
    public $lon;
}