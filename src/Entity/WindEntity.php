<?php
declare(strict_types=1);

namespace RouteeOwpApi\Entity;

class WindEntity extends BaseEntity
{
    public $speed;
    public $deg;
    public $gust;
}