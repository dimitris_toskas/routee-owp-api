<?php
declare(strict_types=1);

namespace RouteeOwpApi\Entity;

use phpDocumentor\Reflection\Types\Integer;

class BaseEntity implements iEntity
{
    public function __construct($data = [])
    {
        if (!empty($data)){
            foreach ($data as $key => $value){
                if (property_exists($this, (string)$key)){
                    $this->$key = $value;
                }
            }
        }
    }
}