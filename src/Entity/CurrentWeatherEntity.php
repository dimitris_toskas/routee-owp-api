<?php
declare(strict_types=1);

namespace RouteeOwpApi\Entity;
/**
 * Class CurrentWeatherEntity
 * @property CoordEntity $coord
 * @property WeatherEntity $weather
 * @property MainEntity $main
 * @property WindEntity $wind
 * @property CloudsEntity $clouds
 * @property SysEntity $sys
 * @package RouteeOwpApi\Entity
 */
class CurrentWeatherEntity extends BaseEntity
{
    public $coord      = null;
    public $weather    = null;
    public $base       = null;
    public $main       = null;
    public $visibility = null;
    public $wind       = null;
    public $clouds     = null;
    public $dt         = null;
    public $sys        = null;
    public $timezone   = null;
    public $id         = null;
    public $name       = null;
    public $cod        = null;

    private $propertiesToEntitiesMap = [
        'coord'   => CoordEntity::class,
        'weather' => WeatherEntity::class,
        'main'    => MainEntity::class,
        'wind'    => WindEntity::class,
        'clouds'  => CloudsEntity::class,
        'sys'     => SysEntity::class,
    ];

    public function __construct($data = [])
    {
        foreach ($this->propertiesToEntitiesMap as $prop => $entityClass) {
            if (isset($data[$prop])){
                $this->$prop = new $entityClass($data[$prop]);
                unset($data[$prop]);
            }
        }
        parent::__construct($data);
    }
}