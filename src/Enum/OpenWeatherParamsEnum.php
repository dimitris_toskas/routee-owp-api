<?php
declare(strict_types=1);

namespace RouteeOwpApi\Enum;

class OpenWeatherParamsEnum
{
    const QUERY    = 'q';
    const APP_ID   = 'appid';
    const UNITS    = 'units';
    const CITY_ID  = 'id';
    const ZIP_CODE = 'zip';
    const LAT      = 'lat';
    const LON      = 'lon';
}