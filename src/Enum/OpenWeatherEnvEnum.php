<?php
declare(strict_types=1);

namespace RouteeOwpApi\Enum;

class OpenWeatherEnvEnum
{
    const OPEN_WEATHER_API_URL = 'OPEN_WEATHER_API_URL';
    const OPEN_WEATHER_API_KEY = 'OPEN_WEATHER_API_KEY';
}