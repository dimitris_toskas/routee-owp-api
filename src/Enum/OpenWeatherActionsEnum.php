<?php
declare(strict_types=1);

namespace RouteeOwpApi\Enum;

class OpenWeatherActionsEnum
{
    const CURRENT_WEATHER = 'weather';
}