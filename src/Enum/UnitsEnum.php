<?php
declare(strict_types=1);

namespace RouteeOwpApi\Enum;

class UnitsEnum
{
    const STANDARD = 'standard';
    const METRIC   = 'metric';
    const IMPERIAL = 'imperial';
}