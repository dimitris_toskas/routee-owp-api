<?php
declare(strict_types=1);

namespace RouteeOwpApi\Request;

use RouteeBaseApi\Enum\HttpMethodEnum;
use RouteeOwpApi\Enum\OpenWeatherActionsEnum;
use RouteeOwpApi\Enum\OpenWeatherParamsEnum;
use RouteeOwpApi\Response\CurrentWeatherResponse;

class CurrentWeatherRequest extends BaseWeatherRequest
{
    public function __construct($url = null, $method = HttpMethodEnum::GET, $params = [], $headers = [])
    {
        parent::__construct($url, $method, $params, $headers);
        $this->setAction(OpenWeatherActionsEnum::CURRENT_WEATHER);
        $this->setResponseType(CurrentWeatherResponse::class);
    }

    public function getByCityName(string $cityName):CurrentWeatherResponse
    {
        $this->setRequiredParams([OpenWeatherParamsEnum::QUERY]);
        $this->setParams([OpenWeatherParamsEnum::QUERY => $cityName]);
        return $this->execute();
    }

    public function getByCityId(int $cityId):CurrentWeatherResponse
    {
        $this->setRequiredParams([OpenWeatherParamsEnum::CITY_ID]);
        $this->setParams([OpenWeatherParamsEnum::CITY_ID => $cityId]);
        return $this->execute();
    }

    public function getByZipCode(string $zip):CurrentWeatherResponse
    {
        $this->setRequiredParams([OpenWeatherParamsEnum::ZIP_CODE]);
        $this->setParams([OpenWeatherParamsEnum::ZIP_CODE => $zip]);
        return $this->execute();
    }

    public function getByCoord($lat, $lon):CurrentWeatherResponse
    {
        $this->setRequiredParams([OpenWeatherParamsEnum::LAT]);
        $this->setRequiredParams([OpenWeatherParamsEnum::LON]);
        $this->setParams([
            OpenWeatherParamsEnum::LAT => $lat,
            OpenWeatherParamsEnum::LON => $lon,
        ]);
        return $this->execute();
    }


}