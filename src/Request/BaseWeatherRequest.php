<?php
declare(strict_types=1);

namespace RouteeOwpApi\Request;

use http\Env;
use RouteeBaseApi\Enum\HttpMethodEnum;
use RouteeBaseApi\Request\BaseRequest;
use RouteeOwpApi\Enum\OpenWeatherParamsEnum;
use RouteeOwpApi\Enum\UnitsEnum;
use RouteeOwpApi\Helpers\EnvHelper;

class BaseWeatherRequest extends BaseRequest
{
    protected $appId = null;
    public function __construct($url = null, $method = HttpMethodEnum::GET, $params = [], $headers = [])
    {
        if (empty($url)){
            $url = !empty(EnvHelper::getOpenWeatherApiUrl())
                ?EnvHelper::getOpenWeatherApiUrl()
                :"https://api.openweathermap.org/data/2.5";
        }
        parent::__construct($url, $method, $params, $headers);
        $this->setRequiredParams([OpenWeatherParamsEnum::APP_ID]);
    }

    public function setAppId($appId):void
    {
        $this->appId = $appId;
    }

    /**
     * Overwrite required params to always include the appid parameter
     * @param array $params
     */
    protected function setRequiredParams(array $params = []): void
    {
        //appid required for all requests on openweathermap.org
        if (!in_array(OpenWeatherParamsEnum::APP_ID, $params)) {
            array_push($params, OpenWeatherParamsEnum::APP_ID);
        }
        parent::setRequiredParams($params);
    }

    /**
     * Overwrite setParams to include automatically the appid if exists on environment
     * @param array $params
     */
    protected function setParams(array $params = [])
    {
        if (!isset($params[OpenWeatherParamsEnum::APP_ID])){
            $appId = !empty($this->appId)?$this->appId:EnvHelper::getOpenWeatherApiKey();
            if (!empty($appId)){
                $params[OpenWeatherParamsEnum::APP_ID] = $appId;
            }
        }

        // Overwrite default units of measurement value to metric
        if (!isset($params[OpenWeatherParamsEnum::UNITS])){
            $params[OpenWeatherParamsEnum::UNITS] = UnitsEnum::METRIC;
        }
        parent::setParams($params);
    }
}