<?php
declare(strict_types=1);

namespace RouteeOwpApi\Helpers;

use RouteeOwpApi\Enum\OpenWeatherEnvEnum;

class EnvHelper
{
    public static function getOpenWeatherApiUrl():string
    {
        return $_ENV[OpenWeatherEnvEnum::OPEN_WEATHER_API_URL]??"";
    }
    public static function setOpenWeatherApiUrl($url):void
    {
        $_ENV[OpenWeatherEnvEnum::OPEN_WEATHER_API_URL] = $url;
    }
    public static function getOpenWeatherApiKey():string
    {
        return $_ENV[OpenWeatherEnvEnum::OPEN_WEATHER_API_KEY]??"";
    }
    public static function setOpenWeatherApiKey($key):void
    {
        $_ENV[OpenWeatherEnvEnum::OPEN_WEATHER_API_KEY] = $key;
    }
}