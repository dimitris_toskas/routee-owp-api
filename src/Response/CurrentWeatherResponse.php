<?php
declare(strict_types=1);

namespace RouteeOwpApi\Response;

use RouteeOwpApi\Entity\CurrentWeatherEntity;

class CurrentWeatherResponse extends BaseWeatherResponse
{
    public function __construct($response)
    {
        parent::__construct($response);
        $this->entityClassToTransform = CurrentWeatherEntity::class;
    }
}