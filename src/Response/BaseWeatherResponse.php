<?php
declare(strict_types=1);

namespace RouteeOwpApi\Response;

use RouteeBaseApi\Response\ApiResponse;

class BaseWeatherResponse extends ApiResponse implements iWeatherResponse
{
    public function __construct($response)
    {
        parent::__construct($response);
        // The API weather if doesnt find a city returns 200 http code with the not found message with in
//        $responseData = $this->getData();
//        if (isset($responseData['cod']) && $responseData['cod'] > 299){
//            $this->isSuccess = false;
//            $this->setMessage($responseData['cod']);
//        }
    }
}